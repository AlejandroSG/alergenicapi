<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysToAlergenicIngredientTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alergenic_ingredient', function (Blueprint $table) {
            $table->foreign('ingredient_id')->references('id')->on('ingredients');
            $table->foreign('alergenic_id')->references('id')->on('alergenics');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alergenic_ingredient', function (Blueprint $table) {
            $table->dropForeign('alergenic_ingredient_ingredient_id_foreign');
            $table->dropForeign('alergenic_ingredient_alergenic_id_foreign');
        });
    }
}
