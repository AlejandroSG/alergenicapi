<?php

namespace App\Http\Controllers;

use App\Ingredient;
use Illuminate\Http\Request;

class IngredientController extends Controller
{
    /**
     * Displays all the ingredients
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function all()
    {
        return response()->json(Ingredient::all(), 200);
    }

    /**
     * Displays a specific ingredient
     *
     * @param int $id The id of an ingredient
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     */
    public function show($id)
    {
        $ingredient = Ingredient::find($id);

        if (!empty($ingredient)) {
            return response()->json($ingredient, 200);
        } else {
            return response()->json(null, 404);
        }
    }

    /**
     * Displays all the alergenics from an ingredient
     *
     * @param int $id The id of an ingredient
     * @return mixed
     */
    public function alergenics($id)
    {
        $ingredient = Ingredient::find($id);

        if (!empty($ingredient)) {
            return response()->json($ingredient->alergenics()->get(), 200);
        } else {
            return response()->json(null, 404);
        }
    }

    /**
     * Displays all the dishes where an ingredient belongs to
     *
     * @param int $id The id of an ingredient
     * @return mixed
     */
    public function dishes($id)
    {
        $ingredient = Ingredient::find($id);

        if (!empty($ingredient)) {
            return response()->json($ingredient->dishes()->get(), 200);
        } else {
            return response()->json(null, 404);
        }
    }

    /**
     * Adds an alergenic to an ingredient
     *
     * @param int $ingredientId The id of an ingredient
     * @param int $alergenicId The id of an alergenic
     * @return \Illuminate\Http\JsonResponse
     */
    public function addAlergenic($ingredientId, $alergenicId)
    {
        try{
            $ingredient = Ingredient::find($ingredientId);
            $ingredient->alergenics()->attach($alergenicId);

            return response()->json($ingredient, 201);
        } catch (\Exception $ex) {
            return response()->json(null, 403);
        }
    }

    /**
     * Adds an ingredient to a dish
     *
     * @param int $ingredientId The id of an ingredient
     * @param int $dishId The id of a dish
     * @return \Illuminate\Http\JsonResponse
     */
    public function addDish($ingredientId, $dishId)
    {
        try{
            $ingredient = Ingredient::find($ingredientId);
            $ingredient->dishes()->attach($dishId);

            return response()->json($ingredient, 201);
        } catch (\Exception $ex) {
            return response()->json(null, 403);
        }
    }

    /**
     * Removes an alergenic from an ingredient
     *
     * @param int $ingredientId The id of an ingredient
     * @param int $alergenicId The id of an alergenic
     * @return \Illuminate\Http\JsonResponse
     */
    public function removeAlergenic($ingredientId, $alergenicId)
    {
        try{
            $ingredient = Ingredient::find($ingredientId);
            $ingredient->alergenics()->detach($alergenicId);

            return response()->json($ingredient, 202);
        } catch (\Exception $ex) {
            return response()->json(null, 403);
        }
    }

    /**
     * Removes an ingredient from a dish
     *
     * @param int $ingredientId The id of an ingredient
     * @param int $dishId The id of a dish
     * @return \Illuminate\Http\JsonResponse
     */
    public function removeDish($ingredientId, $dishId)
    {
        try{
            $ingredient = Ingredient::find($ingredientId);
            $ingredient->dishes()->detach($dishId);

            return response()->json($ingredient, 202);
        } catch (\Exception $ex) {
            return response()->json(null, 403);
        }
    }

    /**
     * Creates an ingredient ($_POST['name'] - required)
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        try {
            $ingredient = Ingredient::create($request->all());

            return response()->json($ingredient, 201);
        } catch (\Exception $ex) {
            return response()->json(null, 403);
        }
    }

    /**
     * Updates an ingredient ($_POST['name'] - required)
     *
     * @param Request $request
     * @param int $id The id of an ingredient
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        try {
            $ingredient = Ingredient::find($id);
            $ingredient->update($request->all());

            return response()->json($ingredient, 200);
        } catch (\Exception $ex) {
            return response()->json(null, 403);
        }
    }

    /**
     * Deletes an ingredient
     *
     * @param int $id The id of an ingredient
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        try {
            $ingredient = Ingredient::find($id);
            $ingredient->delete();

            return response()->json(null, 204);
        } catch (\Exception $ex) {
            return response()->json(null, 403);
        }
    }
}
