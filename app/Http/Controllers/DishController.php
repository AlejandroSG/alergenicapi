<?php

namespace App\Http\Controllers;

use App\Dish;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class DishController extends Controller
{
    /**
     * Displays all the dishes
     *
     * @return Collection|static[]
     */
    public function all()
    {
        return response()->json(Dish::all(), 200);
    }

    /**
     * Displays a specific dish
     *
     * @param int $id The id of a dish
     * @return Collection|\Illuminate\Database\Eloquent\Model
     */
    public function show($id)
    {
        $dish = Dish::find($id);

        if (!empty($dish)) {
            return response()->json($dish, 200);
        } else {
            return response()->json(null, 404);
        }
    }

    /**
     * Displays the ingredients from a dish
     *
     * @param int $id The id of a dish
     * @return mixed
     */
    public function ingredients($id)
    {
        $dish = Dish::find($id);

        if (!empty($dish)) {
            return response()->json($dish->ingredients()->get(), 200);
        } else {
            return response()->json(null, 404);
        }
    }

    /**
     * Displays the alergenics from a dish
     *
     * @param int $id The id of a dish
     * @return Collection|static
     */
    public function alergenics($id)
    {
        $dish = Dish::find($id);

        if (!empty($dish)) {
            $ingredients = $dish->ingredients()->get();
            $out = new Collection();

            foreach ($ingredients as $ingredient) {
                $out = $out->merge($ingredient->alergenics()->get());
            }

            return response()->json($out, 200);
        } else{
            return response()->json(null, 404);
        }
    }

    /**
     * Adds an ingredient to a dish
     *
     * @param int $dishId The id of a dish
     * @param int $ingredientId The id of an ingredient
     * @return \Illuminate\Http\JsonResponse
     */
    public function addIngredient($dishId, $ingredientId)
    {
        try{
            $dish = Dish::find($dishId);
            $dish->ingredients()->attach($ingredientId);

            return response()->json($dish, 201);
        } catch (\Exception $ex) {
            return response()->json(null, 403);
        }
    }

    /**
     * Removes an ingredient from a dish
     *
     * @param int $dishId The id of a dish
     * @param int $ingredientId The id of an ingredient
     * @return \Illuminate\Http\JsonResponse
     */
    public function removeIngredient($dishId, $ingredientId)
    {
        try {
            $dish = Dish::find($dishId);
            $dish->ingredients()->detach($ingredientId);

            return response()->json($dish, 202);
        } catch (\Exception $ex) {
            return response()->json(null, 403);
        }
    }

    /**
     * Creates a dish ($_POST['name'] - required)
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        try{
            $dish = Dish::create($request->all());

            return response()->json($dish, 201);
        } catch (\Exception $ex) {
            return response()->json(null, 403);
        }
    }

    /**
     * Updates a dish ($_POST['name'] - required)
     *
     * @param Request $request
     * @param int $id The id of a dish
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        try {
            $dish = Dish::find($id);
            $dish->update($request->all());

            return response()->json($dish, 200);
        } catch (\Exception $ex) {
            return response()->json(null, 403);
        }
    }

    /**
     * Deletes a dish
     *
     * @param int $id The id of a dish
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        try {
            $dish = Dish::find($id);
            $dish->delete();

            return response()->json(null, 204);
        } catch (\Exception $ex) {
            return response()->json(null, 403);
        }
    }
}