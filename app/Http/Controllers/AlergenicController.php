<?php

namespace App\Http\Controllers;

use App\Alergenic;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class AlergenicController extends Controller
{
    /**
     * Displays all the alergenics
     *
     * @return Collection|static[]
     */
    public function all()
    {
        return response()->json(Alergenic::all(), 200);
    }

    /**
     * Display a specific alergenic
     *
     * @param int $id The id of an alergenic
     * @return Collection|\Illuminate\Database\Eloquent\Model
     */
    public function show($id)
    {
        $alergenic = Alergenic::find($id);

        if (!empty($alergenic)) {
            return response()->json($alergenic, 200);
        } else {
            return response()->json(null, 404);
        }
    }

    /**
     * Display the ingredients where an alergenic belongs to
     *
     * @param int $id The id of an alergenic
     * @return mixed
     */
    public function ingredients($id)
    {
        $alergenic = Alergenic::find($id);

        if (!empty($alergenic)) {
            return response()->json($alergenic->ingredients()->get(), 200);
        } else {
            return response()->json(null, 404);
        }
    }

    /**
     * Display the dishes where an alergenic belongs to
     *
     * @param int $id The id of an alergenic
     * @return Collection|static
     */
    public function dishes($id)
    {
        $alergenic = Alergenic::find($id);

        if (!empty($alergenic)) {
            $ingredients = $alergenic->ingredients()->get();

            $out = new Collection();
            foreach ($ingredients as $ingredient) {
                $out = $out->merge($ingredient->dishes()->get());
            }

            return response()->json($out, 200);
        } else {
            return response()->json(null, 404);
        }
    }

    /**
     * Adds an alergenic to an ingredient
     *
     * @param int $alergenicId The id of an alergenic
     * @param int $ingredientId The id of an ingredient
     * @return \Illuminate\Http\JsonResponse
     */
    public function addToIngredient($alergenicId, $ingredientId)
    {
        try{
            $alergenic = Alergenic::find($alergenicId);
            $alergenic->ingredients()->attach($ingredientId);

            return response()->json($alergenic, 201);
        } catch (\Exception $ex) {
            return response()->json(null, 403);
        }
    }

    /**
     * Removes an alergenic from an ingredient
     *
     * @param int $alergenicId The id of an alergenic
     * @param int $ingredientId The id of an ingredient
     * @return \Illuminate\Http\JsonResponse
     */
    public function removeFromIngredient($alergenicId, $ingredientId)
    {
        try {
            $alergenic = Alergenic::find($alergenicId);
            $alergenic->ingredients()->detach($ingredientId);

            return response()->json($alergenic, 202);
        } catch (\Exception $ex) {
            return response()->json(null, 403);
        }
    }

    /**
     * Creates an alergenic ($_POST['name'] - required)
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        try {
            $alergenic = Alergenic::create($request->all());

            return response()->json($alergenic, 201);
        } catch (\Exception $ex) {
            return response()->json(null, 403);
        }
    }

    /**
     * Updates an alergenic ($_POST['name'] - required)
     *
     * @param Request $request
     * @param int $id The id of an alergenic
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        try {
            $alergenic = Alergenic::find($id);
            $alergenic->update($request->all());

            return response()->json($alergenic, 200);
        } catch (\Exception $ex) {
            return response()->json(null, 403);
        }
    }

    /**
     * Deletes an alergenic
     *
     * @param int $id The id of an alergenic
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        try {
            $alergenic = Alergenic::find($id);
            $alergenic->delete();

            return response()->json(null, 204);
        } catch (\Exception $ex) {
            return response()->json(null, 403);
        }
    }
}
