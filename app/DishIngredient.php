<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class DishIngredient extends Pivot
{
    protected $table = 'dish_ingredient';
    protected $hidden = ['pivot'];
}
