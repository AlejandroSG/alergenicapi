<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class AlergenicIngredient extends Pivot
{
    protected $table = 'alergenic_ingredient';
    protected $hidden = ['pivot'];
}
