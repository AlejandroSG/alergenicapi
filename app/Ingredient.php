<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ingredient extends Model
{
    protected $table = 'ingredients';
    protected $visible = ['id', 'name'];
    protected $fillable = ['id', 'name'];

    public function dishes()
    {
        return $this->belongsToMany('App\Dish')->using('App\DishIngredient');
    }

    public function alergenics()
    {
        return $this->belongsToMany('App\Alergenic')->using('App\AlergenicIngredient');
    }
}
