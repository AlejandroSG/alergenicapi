<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dish extends Model
{
    protected $table = 'dishes';
    protected $visible = ['id', 'name'];
    protected $fillable = ['id', 'name'];

    public function ingredients()
    {
        return $this->belongsToMany('App\Ingredient')->using('App\DishIngredient');
    }
}
