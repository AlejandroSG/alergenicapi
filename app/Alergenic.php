<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alergenic extends Model
{
    protected $table = 'alergenics';
    protected $visible = ['id', 'name'];
    protected $fillable = ['id', 'name'];

    public function ingredients()
    {
        return $this->belongsToMany('App\Ingredient')->using('App\AlergenicIngredient');
    }
}
