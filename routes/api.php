<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'api'], function() {
    /*
     * DISHES
     */
    Route::get('dishes', 'DishController@all');
    Route::get('dishes/{id}', 'DishController@show');
    Route::get('dishes/{id}/ingredients', 'DishController@ingredients');
    Route::get('dishes/{id}/alergenics', 'DishController@alergenics');

    Route::post('dishes', 'DishController@create');
    Route::post('dishes/{id}', 'DishController@update');
    Route::post('dishes/{dishId}/ingredients/{ingredientId}', 'DishController@addIngredient');

    Route::delete('dishes/{id}', 'DishController@delete');
    Route::delete('dishes/{dishId}/ingredients/{ingredientId}', 'DishController@removeIngredient');

    /*
     * INGREDIENTS
     */
    Route::get('ingredients', 'IngredientController@all');
    Route::get('ingredients/{id}', 'IngredientController@show');
    Route::get('ingredients/{id}/dishes', 'IngredientController@dishes');
    Route::get('ingredients/{id}/alergenics', 'IngredientController@alergenics');

    Route::post('ingredients', 'IngredientController@create');
    Route::post('ingredients/{id}', 'IngredientController@update');
    Route::post('ingredients/{ingredientId}/alergenics/{alergenicId}', 'IngredientController@addAlergenic');
    Route::post('ingredients/{ingredientId}/dishes/{dishId}', 'IngredientController@addDish');

    Route::delete('ingredients/{id}', 'IngredientController@delete');
    Route::delete('ingredients/{ingredientId}/alergenics/{alergenicId}', 'IngredientController@removeAlergenic');
    Route::delete('ingredients/{ingredientId}/dishes/{dishId}', 'IngredientController@removeDish');

    /*
     * ALERGENICS
     */
    Route::get('alergenics', 'AlergenicController@all');
    Route::get('alergenics/{id}', 'AlergenicController@show');
    Route::get('alergenics/{id}/ingredients', 'AlergenicController@ingredients');
    Route::get('alergenics/{id}/dishes', 'AlergenicController@dishes');

    Route::post('alergenics', 'AlergenicController@create');
    Route::post('alergenics/{id}', 'AlergenicController@update');
    Route::post('alergenics/{alergenicId}/ingredients/{ingredientId}', 'AlergenicController@addToIngredient');

    Route::delete('alergenics/{id}', 'AlergenicController@delete');
    Route::delete('alergenics/{alergenicId}/ingredients/{ingredientId}', 'AlergenicController@removeFromIngredient');
});
