##Introducción

Para este pequeño proyecto he decidido utilizar el framework Laravel debido a
las facilidades que proporciona a la hora de realizar una API REST con MySQL y a la hora
de realizar test automáticos.

El proyecto consiste de una base de datos en la que hay 3 tablas principales relacionadas
entre sí, con las cuales se puede interaccionar a traves de una API que permite acciones
de tipo CRUD.

Para documentar las funcionalidades de la API he utilizado el paquete de composer f2m2/apidocs,
de tal forma que en la ruta /docs se encuentra una documentación con todas las funcionalidades
de la misma.

##Instalación
A la hora de instalar este proyecto, basta con clonar el repositorio, instalar las dependencias de
composer y renombrar el fichero ```.env.example``` a ```.env```. Una vez hecho esto solo habrá que configurar en
este fichero el acceso a base de datos y crear dicha base de datos en MySQL.
Tras esto, realizar el comando ```php artisan migrate``` para crear todas las tablas necesarias y 
estariamos listos para empezar.

##Tests
Para realizar los test automáticos basta con ejecutar el comando ```composer test```. Hay que tener especial
cuidado ya que realizar los test elimina la base de datos para poder trabajar en limpio, por lo que
hay que realizar los tests ANTES de empezar a trabajar.