<?php

namespace Tests\Feature;

use App\Alergenic;
use App\Dish;
use App\Ingredient;
use Tests\TestCase;

class DishTest extends TestCase
{
    public function testAll()
    {
        factory(Dish::class)->create([
            'name' => 'First Dish'
        ]);

        factory(Dish::class)->create([
            'name' => 'Second Dish'
        ]);

        $this->json('GET', '/api/dishes')
            ->assertStatus(200)
            ->assertJson([
                ['name' => 'First Dish'],
                ['name' => 'Second Dish']
            ]);
    }

    public function testShow()
    {
        $dish = factory(Dish::class)->create([
            'name' => 'Dish'
        ]);

        $this->json('GET', '/api/dishes/' . $dish->id)
            ->assertStatus(200)
            ->assertJson(['name' => 'Dish']);
    }

    public function testIngredients()
    {
        $dish = factory(Dish::class)->create([
            'name' => 'Dish'
        ]);
        $ingredient = factory(Ingredient::class)->create([
            'name' => 'Ingredient'
        ]);
        $dish->ingredients()->attach($ingredient->id);

        $this->json('GET', '/api/dishes/' . $dish->id . '/ingredients')
            ->assertStatus(200)
            ->assertJson([
                ['name' => 'Ingredient']
            ]);
    }

    public function testAlergenics()
    {
        $dish = factory(Dish::class)->create([
            'name' => 'Dish'
        ]);
        $ingredient = factory(Ingredient::class)->create([
            'name' => 'Ingredient'
        ]);
        $alergenic = factory(Alergenic::class)->create([
            'name' => 'Alergenic'
        ]);
        $dish->ingredients()->attach($ingredient->id);
        $ingredient->alergenics()->attach($alergenic->id);

        $this->json('GET', '/api/dishes/' . $dish->id . '/alergenics')
            ->assertStatus(200)
            ->assertJson([
                ['name' => 'Alergenic']
            ]);
    }

    public function testAddIngredient()
    {
        $dish = factory(Dish::class)->create([
            'name' => 'Dish'
        ]);
        $ingredient = factory(Ingredient::class)->create([
            'name' => 'Ingredient'
        ]);

        $this->json('POST', 'api/dishes/' . $dish->id . '/ingredients/' . $ingredient->id)
            ->assertStatus(201)
            ->assertJson(['name' => 'Dish']);
    }

    public function testRemoveIngredient()
    {
        $dish = factory(Dish::class)->create([
            'name' => 'Dish'
        ]);
        $ingredient = factory(Ingredient::class)->create([
            'name' => 'Ingredient'
        ]);
        $dish->ingredients()->attach($ingredient->id);

        $this->json('DELETE', 'api/dishes/' . $dish->id . '/ingredients/' . $ingredient->id)
            ->assertStatus(202)
            ->assertJson(['name' => 'Dish']);
    }

    public function testCreate()
    {
        $payload = [
            'name' => 'Dish'
        ];

        $this->json('POST', '/api/dishes', $payload)
            ->assertStatus(201)
            ->assertJson(['name' => 'Dish']);
    }

    public function testUpdate()
    {
        $dish = factory(Dish::class)->create([
            'name' => 'Dish'
        ]);

        $payload = [
            'name' => 'Updated Dish'
        ];

        $this->json('POST', '/api/dishes/' . $dish->id, $payload)
            ->assertStatus(200)
            ->assertJson(['name' => 'Updated Dish']);
    }

    public function testDelete()
    {
        $dish = factory(Dish::class)->create([
            'name' => 'Dish'
        ]);

        $this->json('DELETE', '/api/dishes/' . $dish->id)
            ->assertStatus(204);
    }
}
