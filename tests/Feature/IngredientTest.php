<?php

namespace Tests\Feature;

use App\Alergenic;
use App\Dish;
use App\Ingredient;
use Tests\TestCase;

class IngredientTest extends TestCase
{
    public function testAll()
    {
        factory(Ingredient::class)->create([
            'name' => 'First Ingredient'
        ]);

        factory(Ingredient::class)->create([
            'name' => 'Second Ingredient'
        ]);

        $this->json('GET', '/api/ingredients')
            ->assertStatus(200)
            ->assertJson([
                ['name' => 'First Ingredient'],
                ['name' => 'Second Ingredient']
            ]);
    }

    public function testShow()
    {
        $ingredient = factory(Ingredient::class)->create([
            'name' => 'Ingredient'
        ]);

        $this->json('GET', '/api/ingredients/' . $ingredient->id)
            ->assertStatus(200)
            ->assertJson(['name' => 'Ingredient']);
    }

    public function testDishes()
    {
        $dish = factory(Dish::class)->create([
            'name' => 'Dish'
        ]);
        $ingredient = factory(Ingredient::class)->create([
            'name' => 'Ingredient'
        ]);
        $ingredient->dishes()->attach($dish->id);

        $this->json('GET', '/api/ingredients/' . $ingredient->id . '/dishes')
            ->assertStatus(200)
            ->assertJson([
                ['name' => 'Dish']
            ]);
    }

    public function testAlergenics()
    {
        $alergenic = factory(Alergenic::class)->create([
            'name' => 'Alergenic'
        ]);
        $ingredient = factory(Ingredient::class)->create([
            'name' => 'Ingredient'
        ]);
        $ingredient->alergenics()->attach($alergenic->id);

        $this->json('GET', '/api/ingredients/' . $ingredient->id . '/alergenics')
            ->assertStatus(200)
            ->assertJson([
                ['name' => 'Alergenic']
            ]);
    }

    public function testAddAlergenic()
    {
        $ingredient = factory(Ingredient::class)->create([
            'name' => 'Ingredient'
        ]);
        $alergenic = factory(Alergenic::class)->create([
            'name' => 'Alergenic'
        ]);

        $this->json('POST', 'api/ingredients/' . $ingredient->id . '/alergenics/' . $alergenic->id)
            ->assertStatus(201)
            ->assertJson(['name' => 'Ingredient']);
    }

    public function testAddDish()
    {
        $ingredient = factory(Ingredient::class)->create([
            'name' => 'Ingredient'
        ]);
        $dish = factory(Dish::class)->create([
            'name' => 'Dish'
        ]);

        $this->json('POST', 'api/ingredients/' . $ingredient->id . '/dishes/' . $dish->id)
            ->assertStatus(201)
            ->assertJson(['name' => 'Ingredient']);
    }

    public function testRemoveAlergenic()
    {
        $ingredient = factory(Ingredient::class)->create([
            'name' => 'Ingredient'
        ]);
        $alergenic = factory(Alergenic::class)->create([
            'name' => 'Alergenic'
        ]);
        $ingredient->alergenics()->attach($alergenic->id);

        $this->json('DELETE', 'api/ingredients/' . $ingredient->id . '/alergenics/' . $alergenic->id)
            ->assertStatus(202)
            ->assertJson(['name' => 'Ingredient']);
    }

    public function testRemoveDish()
    {
        $ingredient = factory(Ingredient::class)->create([
            'name' => 'Ingredient'
        ]);
        $dish = factory(Dish::class)->create([
            'name' => 'Dish'
        ]);
        $ingredient->dishes()->attach($dish->id);

        $this->json('DELETE', 'api/ingredients/' . $ingredient->id . '/dishes/' . $dish->id)
            ->assertStatus(202)
            ->assertJson(['name' => 'Ingredient']);
    }

    public function testCreate()
    {
        $payload = [
            'name' => 'Ingredient'
        ];

        $this->json('POST', '/api/ingredients', $payload)
            ->assertStatus(201)
            ->assertJson(['name' => 'Ingredient']);
    }

    public function testUpdate()
    {
        $ingredient = factory(Ingredient::class)->create([
            'name' => 'Ingredient'
        ]);

        $payload = [
            'name' => 'Updated Ingredient'
        ];

        $this->json('POST', '/api/ingredients/' . $ingredient->id, $payload)
            ->assertStatus(200)
            ->assertJson(['name' => 'Updated Ingredient']);
    }

    public function testDelete()
    {
        $ingredient = factory(Ingredient::class)->create([
            'name' => 'Ingredient'
        ]);

        $this->json('DELETE', '/api/ingredients/' . $ingredient->id)
            ->assertStatus(204);
    }
}
