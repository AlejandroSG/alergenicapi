<?php

namespace Tests\Feature;

use App\Alergenic;
use App\Dish;
use App\Ingredient;
use Tests\TestCase;

class AlergenicTest extends TestCase
{
    public function testAll()
    {
        factory(Alergenic::class)->create([
            'name' => 'First Alergenic'
        ]);

        factory(Alergenic::class)->create([
            'name' => 'Second Alergenic'
        ]);

        $this->json('GET', '/api/alergenics')
            ->assertStatus(200)
            ->assertJson([
                ['name' => 'First Alergenic'],
                ['name' => 'Second Alergenic']
            ]);
    }

    public function testShow()
    {
        $alergenic = factory(Alergenic::class)->create([
            'name' => 'Alergenic'
        ]);

        $this->json('GET', '/api/alergenics/' . $alergenic->id)
            ->assertStatus(200)
            ->assertJson(['name' => 'Alergenic']);
    }

    public function testIngredients()
    {
        $alergenic = factory(Alergenic::class)->create([
            'name' => 'Alergenic'
        ]);
        $ingredient = factory(Ingredient::class)->create([
            'name' => 'Ingredient'
        ]);
        $alergenic->ingredients()->attach($ingredient->id);

        $this->json('GET', '/api/alergenics/' . $alergenic->id . '/ingredients')
            ->assertStatus(200)
            ->assertJson([
                ['name' => 'Ingredient']
            ]);
    }

    public function testDishes()
    {
        $dish = factory(Dish::class)->create([
            'name' => 'Dish'
        ]);
        $ingredient = factory(Ingredient::class)->create([
            'name' => 'Ingredient'
        ]);
        $alergenic = factory(Alergenic::class)->create([
            'name' => 'Alergenic'
        ]);
        $dish->ingredients()->attach($ingredient->id);
        $ingredient->alergenics()->attach($alergenic->id);

        $this->json('GET', '/api/alergenics/' . $alergenic->id . '/dishes')
            ->assertStatus(200)
            ->assertJson([
                ['name' => 'Dish']
            ]);
    }

    public function testAddToIngredient()
    {
        $alergenic = factory(Alergenic::class)->create([
            'name' => 'Alergenic'
        ]);
        $ingredient = factory(Ingredient::class)->create([
            'name' => 'Ingredient'
        ]);

        $this->json('POST', 'api/alergenics/' . $alergenic->id . '/ingredients/' . $ingredient->id)
            ->assertStatus(201)
            ->assertJson(['name' => 'Alergenic']);
    }

    public function testRemoveFromIngredient()
    {
        $alergenic = factory(Alergenic::class)->create([
            'name' => 'Alergenic'
        ]);
        $ingredient = factory(Ingredient::class)->create([
            'name' => 'Ingredient'
        ]);
        $alergenic->ingredients()->attach($ingredient->id);

        $this->json('DELETE', 'api/alergenics/' . $alergenic->id . '/ingredients/' . $ingredient->id)
            ->assertStatus(202)
            ->assertJson(['name' => 'Alergenic']);
    }

    public function testCreate()
    {
        $payload = [
            'name' => 'Alergenic'
        ];

        $this->json('POST', '/api/alergenics', $payload)
            ->assertStatus(201)
            ->assertJson(['name' => 'Alergenic']);
    }

    public function testUpdate()
    {
        $alergenic = factory(Alergenic::class)->create([
            'name' => 'Alergenic'
        ]);

        $payload = [
            'name' => 'Updated Alergenic'
        ];

        $this->json('POST', '/api/alergenics/' . $alergenic->id, $payload)
            ->assertStatus(200)
            ->assertJson(['name' => 'Updated Alergenic']);
    }

    public function testDelete()
    {
        $alergenic = factory(Alergenic::class)->create([
            'name' => 'Alergenic'
        ]);

        $this->json('DELETE', '/api/alergenics/' . $alergenic->id)
            ->assertStatus(204);
    }
}
